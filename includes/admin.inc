<?php

/**
 * @file
 * Admin pages.
 */

/**
 * Menu callback; Return settings page.
 *
 * @param unknown_type $form_state
 */

function affiliate_links_settings_page($form, $form_state) {
  $form['affiliate_links_go'] = array(
    '#type' => 'textfield',
    '#title' => t('Cloaked link conversion endpoint'),
    '#default_value' => variable_get('affiliate_links_go', 'go'),
  );
  $form = system_settings_form($form);
  $form['#submit'][] = 'affiliate_links_settings_page_submit';
  return $form;
}

/**
 * Submit callback for affiliate_links_settings_page().
 */
function affiliate_links_settings_page_submit($form, $form_state) {
  // Make sure the menu path change take place immediately.
  menu_rebuild();
}

/**
 * Menu callback; Return cloaked link report page.
 */
function affiliate_links_report_page($form, $form_state) {
    
  $header = array(
    array('data' => t('Account'), 'field' => 'a.name'),
    array('data' => t('Provider'), 'field' => 'a.provider'),
    array('data' => t('Cloaked link'), 'field' => 'l.lid'),
    array('data' => t('Source'), 'field' => 'l.source'),
    array('data' => t('Destination'), 'field' => 'l.dest'),
    array('data' => t('Count'), 'field' => 'l.count', 'sort' => 'desc'),
  );

   $query = db_select('affiliate_links_link','l');
   $query->leftJoin('affiliate_links_account', 'a','a.accid = l.accid');
   $query = $query->extend('PagerDefault')->limit(10)              //10 results per page
    	       ->extend('TableSort')->orderByHeader($header);
   $query = $query->fields('l', array('lid', 'source', 'dest', 'count'))
            ->fields('a', array('name', 'provider'));
   
   
  //while ($link = db_fetch_object($result)) {
  foreach ($query->execute() as $link) {  
    $form['name'][$link->lid] = array(
        '#type' => 'item',
        '#markup' => check_plain($link->name),
    );
    $form['provider'][$link->lid] = array(
        '#type' => 'item',
        '#markup' => check_plain($link->provider),
    );
    $form['lid'][$link->lid] = array(
        '#type' => 'item',
        '#markup' => check_plain($link->lid),
    );
    $form['source'][$link->lid] = array(
        '#type' => 'item',
        '#markup' => check_url($link->source),
    );
    $form['dest'][$link->lid] = array(
        '#type' => 'item',
        '#markup' => check_url($link->dest),
    );
    $form['count'][$link->lid] = array(
        '#type' => 'item',
        '#markup' => $link->count,
    );
  }
  $form['pager'] = array('#type' => 'item', '#markup' => theme('pager',  array('quantity' => 50)));
  return $form;
}

/**
 * Menu callback; Return provider accounts page.
 */
function affiliate_links_account_page($form, &$form_state) {
  // Return bulk operations page if bulk operations button has been pressed.
  
  if (isset($form_state['values']['op'])) {
    switch ($form_state['values']['op']) {
      case t('Delete'):
        return affiliate_links_account_bulk_delete_page($form,
          $form_state,
          array_keys(array_filter($form_state['values']['checkboxes']))
        );
    }
  }

  $header = array(
    array('data' => t('Account'), 'field' => 'name', 'sort' => 'asc'),
    array('data' => t('Provider'), 'field' => 'provider'),
    array('data' => t('Cloaked'), 'field' => 'cloaked'),
    array('data' => t('Operations')),
  );

  $accounts = array();
  $accounts_val = array();
  
  $query = db_select('affiliate_links_account','a')
             ->extend('PagerDefault')
             ->limit(10)              //10 results per page
        	 ->extend('TableSort')
             ->orderByHeader($header)
             ->fields('a', array('accid', 'name', 'provider', 'cloaked'));
  $result = $query->execute();
  
  $dest = drupal_get_destination();
  foreach ($result as $account) {
    
    $accounts[$account->accid] = '';
    
    $accounts_val[$account->accid] = $account;
    
    $form['name'][$account->accid] = array(
        '#type' => 'item',
        '#markup' => check_plain($account->name),
    );
    $form['provider'][$account->accid] = array(
        '#type' => 'item',
        '#markup' => check_plain($account->provider),
    );
    $form['cloaked'][$account->accid] = array(
        '#type' => 'item',
        '#markup' => $account->cloaked ? t('Yes') : t('No'),
    );
    $form['edit'][$account->accid] = array(
        '#type' => 'item',
        '#markup' => l(t('edit'), 'admin/affiliate-links/' . $account->accid . '/edit',
                       array('query' => $dest)),
    );
  }
  
  $form['checkboxes'] = array(
    '#type' => 'checkboxes',
    '#options' => $accounts,
  );
  
  $form['accounts'] = array(
    '#type' => 'value',
    '#value' => $accounts_val,
  );

  $form['actions']['delete'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
    '#validate' => array('affiliate_links_account_bulk_operations_validate'),
    '#submit' => array('affiliate_links_account_bulk_operations_submit'),
  );
  
  //$form['pager'] = array('#type' => 'item', '#markup' => theme('pager',  array('quantity' => $total,'limit' => 2)));
  $form['#theme'] = 'affiliate_links_account_page';
  return $form;
  
}

/**
 * Validate callback for affiliate_links_account_page().
 */
function affiliate_links_account_bulk_operations_validate($form, &$form_state) {
  if (!count(array_filter($form_state['values']['checkboxes']))) {
    form_set_error('checkboxes', t('No accounts selected.'));
  }
}

/**
 * Submit callback for affiliate_links_account_page().
 */
function affiliate_links_account_bulk_operations_submit($form, &$form_state) {
  // Rebuild form to go to 2nd step, e.g. to show confirmation form.
  $form_state['rebuild'] = TRUE;
}

/**
 * Return a confirmation page for bulk account deletion.
 *
 * @param array $accids
 *   Provider account IDs to delete.
 */
function affiliate_links_account_bulk_delete_page($form, $form_state, $accids) {
  $items = array();
  $info = array();
  foreach ($accids as $accid) {
    $info[$accid] = $form_state['values']['accounts'][$accid];
  }
  foreach ($info as $account) {
    $items[] = t(
      'Account: %name, provider: %provider',
      array('%name' => $account->name, '%provider' => $account->provider)
    );
  }

  $form['accids'] = array(
    '#type' => 'value',
    '#value' => $accids,
  );
  $form['list'] = array(
    '#type' => 'item',
    '#markup' => theme('item_list',array('items' => $items)),
  );
  
  $form['#submit'] = array('affiliate_links_account_delete_page_submit');
  
  return confirm_form(
    $form,
    t('Are you sure you want to delete these provider accounts?'),
    "admin/affiliate-links",
    t('This action cannot be undone.'),
    t('Delete all'),
    t('Cancel')
  );
}

/**
 * Submit callback for affiliate_links_account_bulk_delete_page().
 */
function affiliate_links_account_delete_page_submit($form, &$form_state) {
  $accids = $form_state['values']['accids'];
  db_query("DELETE FROM {affiliate_links_account} WHERE accid IN (:accids)",array(':accids' => $accids));
  db_query("DELETE FROM {affiliate_links_link} WHERE accid IN (:accids)",array(':accids' => $accids));
  db_query("DELETE FROM {affiliate_links_pattern} WHERE accid IN (:accids)",array(':accids' => $accids));
  drupal_set_message(
    format_plural(
      count($accids),
      'The account has been deleted.',
      '@count accounts have been deleted.'
    )
  );
}

/**
 * Menu callback; Return provider accounts add page.
 */
function affiliate_links_account_add_page($form, $form_state) {
  _affiliate_links_account_form($form, $form_state);
  return $form;
}

/**
 * Validate callback for affiliate_links_account_add_page().
 */
function affiliate_links_account_add_page_validate($form, &$form_state) {
  $name = $form_state['values']['name'];
  $provider = $form_state['values']['provider'];
  $result = db_query(
    "SELECT 1 FROM {affiliate_links_account}
    WHERE name = :name AND provider = :provider",
    array(':name' => $name, ':provider' => $provider)
  )->fetchField();
  
  if ((bool) $result) {
    $message = t(
      'The specified name already exists. Please choose a different name.'
    );
    form_set_error('name', $message);
  }
  
  // Invoke each field validation.
  $info = affiliate_links_get_provider_info($provider);
  foreach ($info['fields'] as $field) {
    $field_name = is_array($field) ? $field['name'] : $field;
    affiliate_links_invoke_field('validate', $field_name, $form, $form_state);
  }
}

/**
 * Submit callback for affiliate_links_account_add_page().
 */
function affiliate_links_account_add_page_submit($form, &$form_state) {
    
  $values = $form_state['values'];
  $accid = db_insert('affiliate_links_account')
      ->fields(array('name' => $values['name'],
                'provider' => $values['provider'],
                'cloaked' => $values['cloaked'],
                'data' => serialize(array('fields' => $values['fields']))))
      ->execute();
  
  //$accid = db_last_insert_id('affiliate_links_account', 'accid');
  unset($values['patterns']['add'], $values['patterns']['remove']);

  // Only add URL patterns that are not empty
  $patterns = array_filter($values['patterns']);
  // Clean out parts of url pattern that we don't need such as www and http://
  foreach ($patterns as $key => $pattern) {
    $domain = preg_replace('#^(https?://)?www\.(.+\.)#i', '$2', $pattern);
    $patterns[$key] = $domain;
  }
  
  // Only add URL patterns that are unique.
  $patterns = array_unique($patterns);
  foreach ($patterns as $pattern) {
    db_insert('affiliate_links_pattern')
      ->fields(array('accid' => $accid, 'pattern' => $pattern))
      ->execute();
  }

  // Clear account settings and pattern cache.
  affiliate_links_load_data('all', TRUE);

  drupal_set_message(t('The account has been added.'));
  $form_state['redirect'] = 'admin/affiliate-links';
}

/**
 * Menu callback; Return provider accounts edit page.
 */
function affiliate_links_account_edit_page($form, $form_state, $account = NULL) {
  _affiliate_links_account_form($form, $form_state, $account);
  return $form;
}

/**
 * Validate callback for affiliate_links_account_edit_page().
 */
function affiliate_links_account_edit_page_validate($form, &$form_state) {
  $name = $form_state['values']['name'];
  $provider = $form_state['values']['provider'];
  $account = $form_state['values']['account'];
  if (isset($account->name) && ($name !== $account->name)) {
    $result = db_query(
      "SELECT 1 FROM {affiliate_links_account}
      WHERE name = :name AND provider = :provider",
      array(':name' => $name, ':provider' => $provider)
    )->fetchField();
    if ((bool) $result) {
      $message = t(
        'The specified name already exists. Please choose a different name.'
      );
      form_set_error('name', $message);
    }

    // Invoke each field validation.
    $info = affiliate_links_get_provider_info($provider);
    foreach ($info['fields'] as $field) {
      $field_name = is_array($field) ? $field['name'] : $field;
      affiliate_links_invoke_field(
        'validate', $field_name, $form, $form_state, $account
      );
    }
  }
}

/**
 * Submit callback for affiliate_links_account_edit_page().
 */
function affiliate_links_account_edit_page_submit($form, &$form_state) {
  $values = $form_state['values'];
  $accid = $values['account']->accid;
  db_query(
    "UPDATE {affiliate_links_account}
    SET name = :name, provider = :provider, cloaked = :cloaked, data = :data
    WHERE accid = :accid",
    array(':name' => $values['name'],
    ':provider' => $values['provider'],
    ':cloaked' => $values['cloaked'],
    ':data' => serialize(array('fields' => $values['fields'])),
    ':accid' => $accid)
  );

  db_query("DELETE FROM {affiliate_links_pattern} WHERE accid = :accid", array(':accid' => $accid));
  unset($values['patterns']['add'], $values['patterns']['remove']);

  // Only add URL patterns that are not empty
  $patterns = array_filter($values['patterns']);
  // Clean out parts of url pattern that we don't need such as www and http://
  foreach ($patterns as $key => $pattern) {
    $domain = preg_replace('#^(https?://)?www\.(.+\.)#i', '$2', $pattern);
    $patterns[$key] = $domain;
  }
  // Only add URL patterns that are unique.
  $patterns = array_unique($patterns);
  foreach ($patterns as $pattern) {
    db_query(
      "INSERT INTO {affiliate_links_pattern} SET accid = :accid, pattern = :pattern",
      array(':accid' => $accid, ':pattern' => $pattern)
    );
  }

  // Clear account settings and pattern cache.
  affiliate_links_load_data('all', TRUE);

  // Mark all links of an account to be rebuilt.
  db_query("UPDATE {affiliate_links_link} SET rebuild = 1 WHERE accid = :accid", array(':accid' => $accid));

  drupal_set_message(t('The account has been updated.'));
  $form_state['redirect'] = 'admin/affiliate-links';
}

/**
 * Submit callback for adding more URL patterns to provider account.
 */
function affiliate_links_account_url_patterns_add($form, &$form_state) {
  $values = &$form_state['values'];
  unset($values['patterns']['add'], $values['patterns']['remove']);
  $values['patterns_count'] = count($values['patterns']) + 1;
  $form_state['account'] = $values;
  $form_state['rebuild'] = TRUE;
}

/**
 * Submit callback for removing last URL patterns from provider account.
 */
function affiliate_links_account_url_patterns_remove($form, &$form_state) {
  $values = &$form_state['values'];
  unset($values['patterns']['add'], $values['patterns']['remove']);
  $values['patterns_count'] = count($values['patterns']) - 1;
  $form_state['account'] = $values;
  $form_state['rebuild'] = TRUE;
}

/**
 * Return account form.
 *
 * @param object $account
 *   (optional) Fill form default values with values from this account.
 */
function _affiliate_links_account_form(&$form, &$form_state, $account = NULL) {
  if (isset($form_state['account'])) {
    $account = $form_state['account'] + (array) $account;
  }
    
  $account = (object) $account;
  $providers = affiliate_links_get_providers();
  
  if (isset($form_state['values']['provider'])) {
    $provider = $form_state['values']['provider'];
  }
  else {
    $provider = isset($account->provider) ? $account->provider : current($providers);  
  }
  
  $form['#tree'] = TRUE;
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Account name'),
    '#default_value' => isset($account->name) ? $account->name : '',
    '#required' => TRUE,
  );
  $form['provider'] = array(
    '#type' => 'select',
    '#title' => t('Select a provider for this account'),
    '#options' => drupal_map_assoc($providers),
    '#default_value' => $provider,
    '#ajax' => array(
      'callback' => 'affiliate_links_provider_callback',
      'method' => 'replace',
      'wrapper' => 'affiliate-links-fields-wrapper',
    ),
    '#required' => TRUE,
  );
  
  $form['fields'] = array(
    '#type' => 'fieldset',
    '#title' => t('Account fields'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#prefix' => '<div id="affiliate-links-fields-wrapper">',
    '#suffix' => '</div>',
    '#tree' => TRUE,
  );
  
  $form['patterns'] = array(
    '#type' => 'fieldset',
    '#title' => t('Account Domains'),
    '#description' => t(
      'Enter the domains (excluding both http:// and www) that will trigger link conversion for this account. e.g. example.com'
    ),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#prefix' => '<div id="affiliate-links-patterns-wrapper">',
    '#suffix' => '</div>',
    '#tree' => TRUE,
  );
  
  $form['cloaked'] = array(
    '#type' => 'checkbox',
    '#title' => t('Cloak links from this account?'),
    '#default_value' => isset($account->cloaked) ? $account->cloaked : TRUE,
  );

  $info = affiliate_links_get_provider_info($provider);
  foreach ($info['fields'] as $field) {
    $name = is_array($field) ? $field['name'] : $field;
    $settings = is_array($field) ? $field['form'] : array();
    $form['fields'] += affiliate_links_invoke_field(
      'form', $name, $form_state, $account, $settings
    );
  }
  
  $patterns = isset($account->patterns) ? $account->patterns : array();
  sort($patterns);
  if (isset($form_state['values']['patterns_count'])) {
    $patterns_count = ($form_state['values']['patterns_count']) ? $form_state['values']['patterns_count'] : 1;
  }
  else {
    $patterns_count = $patterns ? count($patterns) : 1;
  }
  
  for ($i = 0; $i < $patterns_count; ++$i) {
    $form['patterns'][$i] = array(
      '#type' => 'textfield',
      '#title' => t('URL pattern'),
      '#default_value' => isset($patterns[$i]) ? $patterns[$i] : '',
    );
  }
  
  $form['patterns']['add'] = array(
    '#type' => 'submit',
    '#value' => t('Add more'),
    '#submit' => array('affiliate_links_account_url_patterns_add'),
    '#ajax' => array(
      'callback' => 'affiliate_links_patterns_callback',
      'wrapper' => 'affiliate-links-patterns-wrapper',
    ),
  );
  
  $form['patterns']['remove'] = array(
    '#type' => 'submit',
    '#value' => t('Remove last'),
    '#submit' => array('affiliate_links_account_url_patterns_remove'),
    '#ajax' => array(
      'callback' => 'affiliate_links_patterns_callback',
      'wrapper' => 'affiliate-links-patterns-wrapper',
    ),
  );

  $form['account'] = array(
    '#type' => 'value',
    '#value' => $account,
  );
  
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
}


/**
 * Menu callback; Update provider required fields via ajax
 */
function affiliate_links_provider_callback($form,$form_state) {
  return $form['fields'];
}

function affiliate_links_patterns_callback($form,$form_state){
    return $form['patterns'];
}

/**
 * Disable validation for all of the form elements.
 */
function _affiliate_links_disable_validation(&$form) {
  foreach (element_children($form) as $child) {
    $form[$child]['#validated'] = TRUE;
    $form[$child]['#element_validate'] = NULL;
    $form[$child]['#needs_validation'] = FALSE;
    _affiliate_links_disable_validation($form[$child]);
  }
}