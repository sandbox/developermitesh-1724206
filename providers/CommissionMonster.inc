<?php

/**
 * @file
 * Linkshare provider.
 */

/**
 * Linkshare provider.
 */
class AffiliateLinksCommissionMonster extends AffiliateLinksProvider {
  /**
   * Override AffiliateLinksProvider::getInfo().
   */
  static function getInfo() {
    return array(
      'fields' => array(
        'offer_id',
        'affiliate_id',
      ),
    );
  }

  /**
   * Override AffiliateLinksProvider::convert().
   *
   * @param string $url
   *   Regular URL to convert.
   *
   * @return string
   *   Affiliate link, cloaked if account is set to cloak link.
   */
  public function convert($url) {
    $offer_id = $this->fields['offer_id'];
    $affiliate_id = $this->fields['affiliate_id'];
    
    if (!empty($offer_id) && !empty($affiliate_id)) {
      $lookup_url = "http://tracking.cmjump.com.au/aff_c?offer_id=$offer_id&aff_id=$affiliate_id&url=$url";
      //$lookup_url = "http://getdeeplink.linksynergy.com/createcustomlink.shtml?token=$api_key&mid=$merchant_id&murl=$url";
      $response = drupal_http_request($lookup_url, array(), 'GET');
      if ($response->code == 200) {
        $new_url = $response->data;
        if (substr($new_url, 0, 7) == 'http://') {
          return $new_url;
        }
      }
    }
    return $url;
  }
}
