<?php

/**
 * @file
 * Linkshare provider.
 */

/**
 * Linkshare provider.
 */
class AffiliateLinksDgmPro extends AffiliateLinksProvider {
  /**
   * Override AffiliateLinksProvider::getInfo().
   */
  static function getInfo() {
    return array(
      'fields' => array(
        'campaign_id',
        'affiliate_id',        
      ),
    );
  }

  /**
   * Override AffiliateLinksProvider::convert().
   *
   * @param string $url
   *   Regular URL to convert.
   *
   * @return string
   *   Affiliate link, cloaked if account is set to cloak link.
   */
  public function convert($url) {
    $campaign_id = $this->fields['campaign_id'];
    $affiliate_id = $this->fields['affiliate_id'];
    if (!empty($api_key) && !empty($merchant_id)) {
      $lookup_url = "http://www.s2d6.com/x/?x=c&z=s&campaignid=$campaign_id&affiliateid=$affiliate_id&k=[NETWORKID]&t=$url";                
      //$lookup_url = "http://getdeeplink.linksynergy.com/createcustomlink.shtml?token=$api_key&mid=$merchant_id&murl=$url";
      $response = drupal_http_request($lookup_url, array(), 'GET');
      if ($response->code == 200) {
        $new_url = $response->data;
        if (substr($new_url, 0, 7) == 'http://') {
          return $new_url;
        }
      }
    }
    return $url;
  }

}
