<?php

/**
 * @file
 * Linkshare provider.
 */

/**
 * Linkshare provider.
 */
class AffiliateLinksDGMPerformance extends AffiliateLinksProvider {
  /**
   * Override AffiliateLinksProvider::getInfo().
   */
  static function getInfo() {
    return array(
      'fields' => array(
        'campaign_id',
        'affiliate_id',
        'merchant_id',
      ),
    );
  }

  /**
   * Override AffiliateLinksProvider::convert().
   *
   * @param string $url
   *   Regular URL to convert.
   *
   * @return string
   *   Affiliate link, cloaked if account is set to cloak link.
   */
  public function convert($url) {
    $campaign_id = $this->fields['campaign_id'];
    $affiliate_id = $this->fields['affiliate_id'];
    $merchant_id = $this->fields['merchant_id'];
    if (!empty($campaign_id) && !empty($merchant_id) && !empty($merchant_id)) {
      $lookup_url = "http://t.dgm-au.com/c/$affiliate_id/$campaign_id/$merchant_id?u=$url";
      //$lookup_url = "http://getdeeplink.linksynergy.com/createcustomlink.shtml?token=$api_key&mid=$merchant_id&murl=$url";
      $response = drupal_http_request($lookup_url, array(), 'GET');
      if ($response->code == 200) {
        $new_url = $response->data;
        if (substr($new_url, 0, 7) == 'http://') {
          return $new_url;
        }
      }
    }
    return $url;
  }
}
