<?php
/**
 * @file
 * Handles outgoing links to various affiliate networks.
 */
  
/**
 * Load all the patterns from the cache or database.
 *
 * @param boolean $reset
 *   Reset the cache.
 *
 * @return array
 *   Array of url patterns.
 */
function affiliate_links_load_data($type = 'all', $reset = TRUE) {
  static $data = NULL;

  if (!isset($data) || $reset) {
    // Retrieve the patterns from the cache.
    if (!$reset && ($cache = cache_get('affiliate_links_data')) && !empty($cache->data)) {
      $data = $cache->data;
    }
    else {
      $data = array(
        'accounts' => array(),
        'patterns' => array(),
      );
      $result = db_query("SELECT * FROM {affiliate_links_account}");
      foreach ($result as $row){
        $data['accounts'][$row->accid] = $row;
      }

      $result = db_query("SELECT accid, pattern FROM {affiliate_links_pattern}");
      foreach ($result as $row){
        $data['patterns'][$row->pattern] = &$data['accounts'][$row->accid];
      }

      // Save the links in the cache.
      cache_set('affiliate_links_data', $data);
    }
  }

  if ($type == 'patterns') {
    return $data['patterns'];
  }
  elseif ($type == 'accounts') {
    return $data['accounts'];
  }
  else {
    return $data;
  }
}

/**
 * Convert a regular URL into affiliate link.
 *
 * @param string $url
 *   Regular URL to convert.
 *
 * @return string
 *   Converted affiliate link.
 */
function affiliate_links_convert($url, $cloaking = NULL, $require_account = TRUE) {
  static $urls = array();

  $account_settings = affiliate_links_find_account($url);

  if ($account_settings) {
    $accid = $account_settings->accid;
    // Check if cloaking is specified by the function caller.
    if ($cloaking === TRUE|| $cloaking === FALSE) {
      $cloaked = $cloaking;
    }
    // Use default cloaking settings.
    else {
      $cloaked = $account_settings->cloaked;
    }
  }
  // No Account found
  elseif (!$require_account) {
    $accid = NULL;
    $cloaked = $cloaking;
  }
  // Return original url if no matching account is found and $require_account
  // is set.
  else {
    return $url;
  }

  $link_key = $cloaked ? 'cloaked' : 'direct';
  // Try to use a statically cached url if possible
  if (!empty($urls[$url][$link_key])) {
    return $urls[$url][$link_key];
  }

  $result = db_query("SELECT lid, dest, rebuild FROM {affiliate_links_link} WHERE source = :source",array(':source' => $url));
  $link = db_fetch_object($result);
  
  if ($link) {
    if ($link->rebuild) {
      $account = affiliate_links_account_load($accid);
      if ($account) {
        $link->dest = $account->convert($url);
      }
      else {
        $link->dest = $url;
      }
      affiliate_links_edit_link($link->lid, $url, $link->dest, $accid);
    }
    $link->cloaked = !empty($account_settings) ? $account_settings->cloaked : FALSE;
  }
  elseif (!$link) {
    $link = new stdClass();
    $account = affiliate_links_account_load($accid);
    $link->dest = $account ? $account->convert($url) : $url;
    $link->lid = affiliate_links_add_link($url, $link->dest, $accid);
  }

  $direct_url = url($link->dest, array('external' => TRUE));
  $cloaked_url = isset($link->lid) ? url(variable_get('affiliate_links_go', 'go') . '/' . $link->lid, array('absolute' => TRUE)) : $direct_url;

  $new_url = array(
    'direct' => $direct_url,
    'cloaked' => $cloaked_url,
  );

  // Save url into static cache for later.
  $urls[$url] = $new_url;

  return $new_url[$link_key];
}

/**
 * Check a url against all active accounts and return the account id.
 *
 * @param string $url
 *   Regular URL to lookup.
 *
 * @return integer
 *   Account id.
 */
function affiliate_links_find_account($url) {
  // Break url into constituent parts.
  $host = parse_url($url, PHP_URL_HOST);
  // Strip out preceding www from the domain
  $domain = preg_replace('#^www\.(.+\.)#i', '$1', $host);

  // Load patterns from cache.
  $patterns = affiliate_links_load_data('patterns');

  if (isset($patterns[$domain])) {
    return $patterns[$domain];
  }
}

/**
 * Add an URL into lookup table, together with its converted form.
 *
 * @param string $source
 *   Source URL before conversion.
 * @param string $dest
 *   Destination URL after conversion.
 * @param string $accid
 *   Account Id.
 */
function affiliate_links_add_link($source, $dest, $accid = NULL) {
  $lid = md5(uniqid(mt_rand(), TRUE));

  db_query("INSERT INTO {affiliate_links_link}
    SET lid = :lid, accid = :accid, source = :source, dest = :dest",
    array(':lid' => $lid, ':accid' => $accid, ':source' => $source, ':dest' => $dest));

  return $lid;
}

/**
 * Edit a URL in the lookup table, together with its converted form.
 *
 * @param string $lid
 *   Link Id to lookup the link.
 * @param string $source
 *   Source URL before conversion.
 * @param string $dest
 *   Destination URL after conversion.
 * @param string $accid
 *   Account Id.
 */
function affiliate_links_edit_link($lid, $source, $dest, $accid = NULL) {
  db_query(
    "UPDATE {affiliate_links_link}
    SET accid = :accid, source = :source, dest = :dest, rebuild = 0 WHERE lid = :lid",
    array(':accid' => $accid, ':source' => $source, ':dest' => $dest, ':lid' => $lid));
}

/**
 * Redirect a cloaked link to its destination.
 *
 * @param string $lid
 *   Cloaked link ID.
 * @param boolean $return
 *   TRUE to return the link destination instead of redirect.
 *
 * @return string
 *   Destination URL if $return is TRUE, NULL otherwise.
 */
function affiliate_links_redirect_link($lid, $return = FALSE) {
  affiliate_links_track_link($lid);

  $dest = db_query("SELECT dest FROM {affiliate_links_link} WHERE lid = :lid",array(':lid' =>  $lid))->fetchField();
  //$dest = db_result($result);
  if ($return) {
    return url($dest, array('external' => TRUE));
  }
  else {
    drupal_goto($dest);
  }
}

/**
 * Track click count of a cloaked link.
 *
 * @param string $lid
 *   Cloaked link ID whose click count will be incremented.
 */
function affiliate_links_track_link($lid) {
  db_query("UPDATE {affiliate_links_link} SET count = count + 1 WHERE lid = :lid",array(':lid' => $lid));
}

/**
 * Get list of providers found in module.
 */
function affiliate_links_get_providers() {
  static $providers = NULL;

  if ($providers !== NULL) {
    return $providers;
  }
  
  $files =  file_scan_directory(drupal_get_path('module', 'affiliate_links') . '/providers', '/\.inc$/', array('key' => 'name'), 0);
  
  $providers = array_keys($files);
  sort($providers);
  return $providers;
}

/**
 * Get provider info.
 *
 * @param string $name
 *   Provider name.
 *
 * @return array
 *   Provider info.
 */
function affiliate_links_get_provider_info($name) {
  affiliate_links_include_provider($name);
  return call_user_func("AffiliateLinks$name::getInfo");
}

/**
 * Invoke an operation on a field.
 *
 * @param string $method
 *   Method to invoke.
 * @param string $field
 *   Field name.
 * @param ...
 *   (optional) Additional parameters to pass into field method.
 *
 * @return mixed
 *   Return value from field method, NULL if method not supported by field.
 */
function affiliate_links_invoke_field($method, $field) {
  $params = func_get_args();
  array_splice($params, 0, 2);
    
  affiliate_links_include($field, 'fields');
  $func = "affiliate_links_{$field}_$method";
  if (function_exists($func)) {
    return call_user_func_array($func, $params);
  }
  return NULL;
}

/**
 * Load a provider account.
 *
 * @param int $accid
 *   Account ID to load.
 *
 * @return object
 *   Provider account.
 */
function affiliate_links_account_load($accid) {
  $accounts = affiliate_links_load_data('accounts');
  $account = $accounts[$accid];
  if (!empty($account)) {
    $provider = $account->provider;
    affiliate_links_include_provider($provider);
    $provider = "AffiliateLinks$provider";
    return new $provider($accid);
  }
}
/**
 * Implements hook_menu().
 */
function affiliate_links_menu() {
    
  $items['admin/affiliate-links'] = array(
    'title' => 'Affiliate links',
    'description' => 'Configure affiliate links accounts.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('affiliate_links_account_page'),
    'access arguments' => array('administer affiliate links'),
    'position' => 'left',
    'weight' => -5,
    'file' => 'admin.inc',
    'file path' => drupal_get_path('module', 'affiliate_links').'/includes',
    'type' => MENU_NORMAL_ITEM,
  );
  
  $items['admin/affiliate-links/list'] = array(
    'title' => 'Accounts',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );
  $items['admin/affiliate-links/add'] = array(
    'title' => 'Add account',
    'description' => 'Add new provider account.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('affiliate_links_account_add_page'),
    'access arguments' => array('administer affiliate links'),
    'file' => 'admin.inc',
    'file path' => drupal_get_path('module', 'affiliate_links').'/includes',
    'type' => MENU_LOCAL_TASK,
  );
  $items['admin/affiliate-links/settings'] = array(
    'title' => 'Settings',
    'description' => 'Configure affiliate links settings.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('affiliate_links_settings_page'),
    'access arguments' => array('administer affiliate links'),
    'file' => 'admin.inc',
    'file path' => drupal_get_path('module', 'affiliate_links').'/includes',
    'type' => MENU_LOCAL_TASK,
  );
  $items["admin/affiliate-links/%affiliate_links_account/edit"] = array(
    'title' => 'Edit provider account',
    'description' => 'Edit an existing provider account.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('affiliate_links_account_edit_page', 2),
    'access arguments' => array('administer affiliate links'),
    'file' => 'admin.inc',
    'file path' => drupal_get_path('module', 'affiliate_links').'/includes',
    'type' => MENU_CALLBACK,
  );

  $items['admin/reports/affiliate-links'] = array(
    'title' => 'Affiliate links statistic',
    'description' => 'Affiliate links cloaked link statistic.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('affiliate_links_report_page'),
    'access arguments' => array('access site reports'),
    'file' => 'admin.inc',
    'file path' => drupal_get_path('module', 'affiliate_links').'/includes',
    'type' => MENU_NORMAL_ITEM,
  );
  
  $items[variable_get('affiliate_links_go', 'go')] = array(
    'page callback' => 'affiliate_links_redirect_link',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );
  return $items;
}

/**
 * Implements hook_theme().
 */
function affiliate_links_theme() {
  return array(
    'affiliate_links_account_page' => array(
      'arguments' => array('form' => NULL),
      'render element' => 'form',
      'file' => 'includes/admin.theme.inc',
    ),
    'affiliate_links_report_page' => array(
      'arguments' => array('form' => NULL),
      'render element' => 'form',
      'file' => 'includes/admin.theme.inc',
    ),
    'affiliate_links_formatter_affiliate_plain' => array(
      'variables' => array('element' => NULL),
    ),
    'affiliate_links_formatter_affiliate_url' => array(
      'variables' => array('element' => NULL),
    ),
    'affiliate_links_formatter_affiliate_short' => array(
      'variables' => array('element' => NULL),
    ),
    'affiliate_links_formatter_affiliate_label' => array(
      'variables' => array('element' => NULL),
    ),
    'affiliate_links_formatter_affiliate_separate' => array(
      'variables' => array('element' => NULL),
    ),
  );
  return $theme;
}

/**
 * Implements hook_perm().
 */
function affiliate_links_permission() {
  return array(
    'administer affiliate links' => array(
      'title' => t('Administer affiliates'),
      'description' => t(''),
    ));
}

/**
 * Implements hook_filter().
 */
function affiliate_links_filter($op, $delta = 0, $format = -1, $text = '') {
  switch ($op) {
    case 'list':
      return array(
        0 => t('Affiliate Links'),
      );

    case 'description':
      return t('Converts urls into affiliate urls based on the account settings.');

    case 'process':
      return affiliate_links_filter_process($text, $format);

    case 'settings':
      return affiliate_links_filter_settings($format);

    default:
      return $text;
  }
}

/**
 * Implements of hook_filter_tips().
 */
function affiliate_links_filter_tips($delta, $format, $long = FALSE) {
  if ($long) {
    return t('
      <p><strong>Affiliate Links</strong></p>
      <p>Links for websites where an affiliate account has been configured will be automatically
      converted to a link containing the relevant affiliate tracking code.</p>');
  }
  else {
    return t('Links will automatically converted into Affiliate Links where appropriate.');
  }
}

/**
 * Implements hook_field_formatter_info().
 */
function affiliate_links_field_formatter_info() {
  return array(
    'affiliate_url' => array(
      'label' => t('Affiliate Links:') . ' ' . t('URL, as link'),
      'field types' => array('link'),
      'multiple values' => CONTENT_HANDLE_CORE,
    ),
    'affiliate_plain' => array(
      'label' => t('Affiliate Links:') . ' ' . t('URL, as plain text'),
      'field types' => array('link'),
      'multiple values' => CONTENT_HANDLE_CORE,
    ),
    'affiliate_short' => array(
      'label' => t('Affiliate Links:') . ' ' . t('Short, as link with title "Link"'),
      'field types' => array('link'),
      'multiple values' => CONTENT_HANDLE_CORE,
    ),
    'affiliate_label' => array(
      'label' => t('Affiliate Links:') . ' ' . t('Label, as link with label as title'),
      'field types' => array('link'),
      'multiple values' => CONTENT_HANDLE_CORE,
    ),
    'affiliate_separate' => array(
      'label' => t('Affiliate Links:') . ' ' . t('Separate title and URL'),
      'field types' => array('link'),
      'multiple values' => CONTENT_HANDLE_CORE,
    ),
  );
}

/**
 * Include affiliate links module include file.
 *
 * @param string $file
 *   Name of file to include without the .inc extension.
 * @param string $dir
 *   (optional) The directory to include the file from. Default to 'includes'.
 */
function affiliate_links_include($file, $dir = 'includes') {
  include_once drupal_get_path('module', 'affiliate_links') . "/$dir/$file.inc";
}

/**
 * Include an affiliate links provider interface.
 *
 * @param string $name
 *   Provider name, this is also the same as its file name.
 * @param string $dir
 *   (optional) The directory to include the file from. Default to 'providers'.
 */
function affiliate_links_include_provider($name, $dir = 'providers') {
  affiliate_links_include('provider');
  affiliate_links_include($name, $dir);
}

/**
 * Process operation callback for hook_filter.
 * Locates all urls inside links, and runs them through affiliate_links_convert.
 *
 * @param string $text
 *    Text to process
 * @param int $format
 *    Input format corresponding to the filter configuration.
 */
function affiliate_links_filter_process($text, $format = -1) {
  $cloaking = variable_get('affiliate_links_cloaking_' . $format, 'default');
  switch ($cloaking) {
    case 'force':
      $cloaking = 'TRUE';
      break;
    case 'disable':
      $cloaking = 'FALSE';
      break;
    default:
      $cloaking = 'NULL';
      break;
  }

  $text = preg_replace_callback(
    '/(href=["\'])((https|http):\/\/\.?[^"\'\s]*)/i',
     create_function('$matches', 'return $matches[1] . affiliate_links_convert($matches[2], ' . $cloaking . ');'),
    $text
  );

  return $text;
}

/**
 * Generates a settings form for configuring the Affiliate Links filter
 * @param int $format
 *    Input format being configured.
 * @return
 *    Form API array.
 */
function affiliate_links_filter_settings($format) {
  $form['affiliate_links'] = array(
    '#type' => 'fieldset',
    '#title' => t('Affiliate links filter'),
    '#collapsible' => TRUE,
  );

  $form['affiliate_links']['affiliate_links_cloaking_'.$format] = array(
    '#type' => 'select',
    '#title' => t('Cloaking / Redirect settings'),
    '#options' => array(
      'default' => t('Default (as set in account)'),
      'force' => t('All links cloaked'),
      'disable' => t('No links cloaked'),
    ),
    '#default_value' => variable_get('affiliate_links_cloaking_'.$format, 'default'),
  );

  return $form;
}

/**
 * Convert a link field element url into an affiliate url.
 *
 * @param object &$element
 *   Referenced element containing the regular URL to convert.
 */
function affiliate_links_convert_linkfield(&$element) {
  $url = $element['#item']['url'];
  // Link field stores url and query string separately, so we need to join them together.
  $url .= !empty($element['#item']['query']) ? '?' . $element['#item']['query'] : '';
  $url = affiliate_links_convert($url);
  // Now we need to split them back up.
  $url = explode('?', $url);
  $element['#item']['url'] = $url[0];
  $element['#item']['query'] = !empty($url[1]) ? $url[1] : '';
}

/**
 * Theme function for 'plain' text field formatter.
 * Acts as a pass-through to the corresponding link field formatter.
 */
function theme_affiliate_links_formatter_affiliate_plain($element) {
  affiliate_links_convert_linkfield($element);
  return theme('link_formatter_plain', $element);
}

/**
 * Theme function for 'url' text field formatter.
 * Acts as a pass-through to the corresponding link field formatter.
 */
function theme_affiliate_links_formatter_affiliate_url($element) {
  affiliate_links_convert_linkfield($element);
  return theme('link_formatter_url', $element);
}

/**
 * Theme function for 'short' text field formatter.
 * Acts as a pass-through to the corresponding link field formatter.
 */
function theme_affiliate_links_formatter_affiliate_short($element) {
  affiliate_links_convert_linkfield($element);
  return theme('link_formatter_short', $element);
}

/**
 * Theme function for 'label' text field formatter.
 * Acts as a pass-through to the corresponding link field formatter.
 */
function theme_affiliate_links_formatter_affiliate_label($element) {
  affiliate_links_convert_linkfield($element);
  return theme('link_formatter_label', $element);
}

/**
 * Theme function for 'separate' text field formatter.
 * Acts as a pass-through to the corresponding link field formatter.
 */
function theme_affiliate_links_formatter_affiliate_separate($element) {
  affiliate_links_convert_linkfield($element);
  return theme('link_formatter_separate', $element);
}