<?php

/**
 * @file
 * Affiliate ID field.
 */

/**
 * Return form element for this field.
 */
function affiliate_links_offer_id_form($form_state, $account = NULL, $settings = array()) {
  $form['offer_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Offer ID'),
    '#default_value' => isset($account->fields['offer_id']) ?
      $account->fields['offer_id'] : '',
    '#required' => TRUE,
  );
  $form['offer_id'] = $settings + $form['offer_id'];
  return $form;
}

/**
 * Validate callback for affiliate_links_offer_id_form().
 */
function affiliate_links_offer_id_validate($form, $form_state,$account = NULL) {

}
