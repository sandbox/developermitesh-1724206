<?php

/**
 * @file
 * Merchant ID field.
 */

/**
 * Return form element for this field.
 */
function affiliate_links_merchant_id_form($form_state, $account = NULL, $settings = array()) {
  $form['merchant_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Merchant ID'),
    '#default_value' => isset($account->fields['merchant_id']) ?
      $account->fields['merchant_id'] : '',
    '#required' => TRUE,
  );
  $form['merchant_id'] = $settings + $form['merchant_id'];
  return $form;
}

/**
 * Validate callback for affiliate_links_merchant_id_form().
 */
function affiliate_links_merchant_id_validate($form, $form_state, $account = NULL) {

}
