<?php

/**
 * @file
 * Affiliate ID field.
 */

/**
 * Return form element for this field.
 */
function affiliate_links_campaign_id_form($form_state, $account = NULL, $settings = array()) {
  $form['campaign_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Campaign ID'),
    '#default_value' => isset($account->fields['campaign_id']) ?
      $account->fields['campaign_id'] : '',
    '#required' => TRUE,
  );
  $form['campaign_id'] = $settings + $form['campaign_id'];
  return $form;
}

/**
 * Validate callback for affiliate_links_campaign_id_form().
 */
function affiliate_links_campaign_id_validate($form, $form_state,$account = NULL) {

}
